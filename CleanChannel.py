import random
import logging
import sys
from telethon import TelegramClient, sync
from telethon import functions, types
from telethon.sync import TelegramClient
from telethon.tl.functions.messages import CheckChatInviteRequest
from telethon.tl.functions.channels import EditBannedRequest
from telethon.tl.types import InputChannel
from telethon.tl.types import ChatBannedRights
import pandas as pd
import xlsxwriter

api_id = "YOUR_API_ID"
api_hash = "YOUR_API_HASH"
channel = "END_OF_CHANNEL_LINK"
phone_number = "USERBOT_PHONE"
client = TelegramClient("CleanChannel", api_id, api_hash)
client.connect()
chatinvite = client(CheckChatInviteRequest(channel))
channel_id = chatinvite.chat.id
access_hash_channel = chatinvite.chat.access_hash
input_channel = InputChannel(channel_id, access_hash_channel)
client.disconnect()
logging.basicConfig(format='%(asctime)s %(levelname)s %(message)s',
    datefmt='%d-%m-%Y:%H:%M:%S',
    level=logging.DEBUG,
    filename='CleanChannel.log', filemode='a')
logger = logging.getLogger('bot_logger')
logger.info("Starting bot ...")
data_file = pd.ExcelFile("./customer.xlsx")
data_sheet = pd.read_excel(data_file, header=None)
workbook = xlsxwriter.Workbook('./exception.xlsx')
worksheet = workbook.add_worksheet("Data")
row_position = 0
column_position = 0


def get_channel_member():
    client.connect()
    channel_member_chatID = []
    logger.info("Get channel member:")
    try:
        for u in client.iter_participants(input_channel, aggressive=True):
            if u.id not in channel_member_chatID:
                channel_member_chatID.append((u.id, u.first_name))
            logger.info(f"{u.first_name}: {u.id}")
        logger.info(channel_member_chatID)
        return channel_member_chatID
    except Exception as e:
        logger.error("Can't get channel member :")
        logger.error(f"exx { e }")
        return False

def get_all_contacts():
    client.connect()
    try:
        contacts = client(functions.contacts.GetContactsRequest(hash=0))
        logger.info("Contacts")
        logger.info(contacts.stringify())
        customers_id = []
        for cid in contacts.users:
            customers_id.append(cid.id)
        return customers_id
    except Exception as e:
        logger.error("Can't get all contacts:")
        logger.error(f"exx { e }")
        return False        

def import_contacts(phone_list):
    if not phone_list:
        return False
    client.connect()
    contact_obj_list = []
    counter = 1 
    for phone in phone_list:
        contact_obj = types.InputPhoneContact(
            client_id=random.randrange(-(2 ** 63), 2 ** 63),
            phone=str(phone),
            first_name=f"USER { counter }",
            last_name=str(phone),
        )
        if contact_obj != None:
            contact_obj_list.append(contact_obj)
            counter += 1
        else:
            logger.error(f"Can't import this number: {phone}")
    rsl = ""
    try:
        rsl = client(
                functions.contacts.ImportContactsRequest(
                    contacts=contact_obj_list
                )
            )
        logger.info(f"Import contacts:")
        logger.info(rsl.stringify())
        client_id_list = []
        for imported_obj in rsl.imported:
            client_id_list.append(imported_obj.client_id)
        for co in contact_obj_list:
            if co.client_id not in client_id_list:
                global row_position
                global column_position
                worksheet.write(row_position, column_position, co.phone) 
                worksheet.write(row_position, column_position + 1, "ImportError") 
                row_position += 1
                column_position = 0
        return rsl
    except Exception as e:
        logger.error("Can't import contacts:")
        logger.error(rsl.stringify())   
        logger.error(f"exx { e }")
        return False

def clean_contacts(contactID_list):
    if not contactID_list:
        logger.error("empty contactID_list")
        return False
    client.connect()
    try:
        delete_contact = client(functions.contacts.DeleteContactsRequest(
            id = contactID_list
        ))
        logger.info("Clean contact")
        logger.info(delete_contact.stringify())
    except Exception as e:
        logger.error("Can't clean contacts:")
        logger.error(f"exx { e }")
        return False  
  
def clean_channel(channel_member_chatID, customers_chatID):
    if not channel_member_chatID and not customers_chatID:
        logger.error("empty channel_member_chatID and empty customers chatID")
        return False
    for userID, user_name in channel_member_chatID:
        client.connect()
        try:
            if userID not in customers_chatID:
                remove = client(EditBannedRequest(
                    input_channel, userID, ChatBannedRights(
                        until_date=None,
                        view_messages=True
                    )
                ))
                logger.info(f"{ user_name } : { userID } removed")
                logger.info(remove.stringify())
            else:
                logger.info(f"{ user_name } { userID } is our customer")
        except Exception as e:
            global row_position
            global column_position
            worksheet.write(row_position, column_position, f"{ user_name }: { userID }") 
            worksheet.write(row_position, column_position + 1, "RemoveError") 
            row_position += 1
            logger.error(f"Can't remove { userID } : { user_name }")
            logger.error(f"exx { e }")
        client.disconnect()





if __name__ == "__main__":
    client.connect()
    logger.info("Connected")
    chatinvite = client(CheckChatInviteRequest('KKATshuh0y9iMTZk'))
    channel_id = chatinvite.chat.id
    access_hash_channel = chatinvite.chat.access_hash
    input_channel = InputChannel(channel_id, access_hash_channel)
    data_file = pd.ExcelFile("./customer.xlsx")
    data_sheet = pd.read_excel(data_file, header=None)
    if not client.is_user_authorized():
        client.send_code_request(phone_number)
        me = client.sign_in(phone_number, input("Enter code: "))

    customer_number_xlsx = []
    for number in data_sheet.values:
        customer_number_xlsx.append(number[0])   

    contacts = get_all_contacts()
    clean_contacts(contacts)
    channel_member_chatID = get_channel_member()
    import_contacts(customer_number_xlsx)
    new_imported_contacts = get_all_contacts()
    clean_channel(channel_member_chatID, new_imported_contacts)
    workbook.close()
